package com.example.app.demo.service;

import com.example.app.demo.dto.CreateUserDto;
import com.example.app.demo.dto.UserDto;
import com.example.app.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    private List<User> users = new ArrayList<>();
    @Autowired
    UserMapperDto userMapperDto;

    @PostConstruct
    public void init() {
        users.add(new User("sulik","qwerty","Kuba","Nowak"));
        users.add(new User("krol1","qazedc","Izabela","Kowalska"));
    }

    public List<UserDto> getAllUsers(){
        List<UserDto> userDtos = new ArrayList<>();
        for (User u : users){
            userDtos.add(userMapperDto.mapToDto(u));
        }
        return userDtos;
    }
    public UserDto searchUserByLogin(String login) throws UserNotFound{
        for (User u : users){
            if (u.getLogin().equals(login)){
                return userMapperDto.mapToDto(u);
            }
        }
        throw new UserNotFound();
    }
    public UserDto createUser(CreateUserDto createUserDto)throws UserAlreadyExsist, UserInvalidData{
        if (createUserDto.getLogin().toCharArray().length<3||createUserDto.getLogin()==null){
            throw new UserInvalidData();
        }
        for (User user : users){
            if (user.getLogin().equals(createUserDto.getLogin())){
                throw new UserAlreadyExsist();
            }
        }
        User user = userMapperDto.mapToModel(createUserDto);
        users.add(user);
        return userMapperDto.mapToDto(user);

    }
    public UserDto updateUser(CreateUserDto createUserDto) throws UserInvalidData, UserNotFound{
        if (createUserDto.getLogin()==null||createUserDto.getLogin().toCharArray().length<3){
            throw new UserInvalidData();
        }
        for (User user : users){
            if (user.getLogin().equals(createUserDto.getLogin())){
                user.setLogin(createUserDto.getLogin());
                user.setPassword(createUserDto.getPassword());
                user.setName(createUserDto.getName());
                user.setSurname(createUserDto.getSurname());
                return userMapperDto.mapToDto(user);

            }
        }
        throw new UserNotFound();
    }
    public UserDto deleteUser(String login) throws UserNotFound{
        for (User user : users){
            if (user.getLogin().equals(login)){
                users.remove(user);
                return userMapperDto.mapToDto(user);
            }
        }
        throw new UserNotFound();
    }
}