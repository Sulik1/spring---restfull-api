package com.example.app.demo.service;

import com.example.app.demo.dto.CreateUserDto;
import com.example.app.demo.dto.UserDto;
import com.example.app.demo.model.User;
import org.springframework.stereotype.Service;

@Service
public class UserMapperDto {
    public User mapToModel(CreateUserDto userDto){
        return new User(userDto.getLogin(),userDto.getPassword(),userDto.getSurname(),userDto.getSurname());
    }
    public UserDto mapToDto(User user){
        return new UserDto(user.getLogin(),user.getPassword(),user.getName(),user.getSurname());
    }

}
