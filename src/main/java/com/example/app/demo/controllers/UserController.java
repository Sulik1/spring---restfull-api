package com.example.app.demo.controllers;

import com.example.app.demo.dto.CreateUserDto;
import com.example.app.demo.dto.UserDto;
import com.example.app.demo.service.UserAlreadyExsist;
import com.example.app.demo.service.UserInvalidData;
import com.example.app.demo.service.UserNotFound;
import com.example.app.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping
    public List<UserDto> getAllUsers(){
        return userService.getAllUsers();
    }
    @GetMapping("/{login}")
    public UserDto getUserByLogin(@PathVariable String login) throws UserNotFound {
        return userService.searchUserByLogin(login);
    }
    @PostMapping
    public UserDto createUser(@RequestBody CreateUserDto createUserDto) throws UserInvalidData, UserAlreadyExsist {
        return userService.createUser(createUserDto);
    }
    @PutMapping
    public UserDto updateUser(@RequestBody CreateUserDto createUserDto) throws UserNotFound, UserInvalidData {
        return userService.updateUser(createUserDto);
    }

    @DeleteMapping("/{login}")
    public UserDto deleteUser(@PathVariable String login) throws UserNotFound {
        return userService.deleteUser(login);
    }

}
